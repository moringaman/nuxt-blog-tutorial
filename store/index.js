import Vuex from 'vuex'
import Cookie from 'js-cookie'

const createStore = () => {
    return new Vuex.Store({
        state: {
            loadedPosts: [],
            token: null
        },
        mutations: {
            setPosts(state, posts) {
                state.loadedPosts = posts
            },
            editPost(state, editedPost) {
                const postIndex = state.loadedPosts.findIndex(
                    post => post.id === editedPost.id
                )
                state.loadedPosts[postIndex] = editedPost 
            },
            addPost(state, post){
                state.loadedPosts.push(post)
            },
            setToken(state, token){
                state.token = token
            },
            clearToken(state) {
                state.token = null
            }
        },
        actions: {
            nuxtServerInit(vuexContext, context) {
                // console.log(context.app.$axios)
               return context.app.$axios
               .$get('/posts.json')
                   .then(data => {
                      const postsArray = []
                      for (const key in data) {
                        postsArray.push({...data[key], id: key})
                      }
                      vuexContext.commit('setPosts', postsArray)
                   })
                   .catch(e => context.error(e))
            },
            addPost(vuexContext, post){
                const createdPost = {
                    ...post,
                     updatedDate: new Date()
                     }
               return this.$axios.$post(`/posts.json?auth=${vuexContext.state.token}`, createdPost)
            .then(data => {
             vuexContext.commit('addPost', {...createdPost, id: data.name} )
            })
            .catch(error => {
              console.log(error)
            })
            },
            editPost(vuexContext, editedPost){
                return this.$axios.$put(`/posts/${editedPost.id}.json?auth=${vuexContext.state.token}`, editedPost)
                .then(res=> {
                    vuexContext.commit('editPost', editedPost)
                })
                .catch(e => console.log(e))
            },
            authenticateUser(vuexContext, authData) {
                let Auth_URL = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key='
                if (!authData.isLogin) {
                   Auth_URL = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key="
                }
                return this.$axios
                .$post(Auth_URL + process.env.fbApiKey, {
                  email: authData.email,
                  password: authData.password,
                  returnSecureToken: true
                })
                .then(result=>{
                    vuexContext.commit('setToken', result.idToken)
                    localStorage.setItem('token', result.idToken)
                    localStorage.setItem('tokenExpiration', new Date().getTime() + +result.expiresIn * 1000)
                    Cookie.set('jwt', result.idToken)
                    Cookie.set('tokenExpiration', new Date().getTime() + +result.expiresIn * 1000)
                  //console.log(result)
                  return this.$axios.$post('http://localhost:3000/api/track-data', {data: "authenticated"})
                })
                .catch(err => {
                  console.log(err)
                })
            },
            initAuth(vuexContext, req){
                let token;
                let expiresIn;
                if (req) {
                    if (!req.headers.cookie) {
                        return
                    }
                    const jwtCookie = req.headers.cookie.split(';')
                    .find(c => c.trim()
                    .startsWith('jwt='))
                    if (!jwtCookie) {
                        return
                    }
                     token = jwtCookie.split('=')[1]
                     expiresIn= req.headers.cookie.split(';')
                    .find(c => c.trim()
                    .startsWith('tokenExpiration='))
                    .split('=')[1]

                } else {
                     token = localStorage.getItem('token')
                     expiresIn = localStorage.getItem('tokenExpiration')
                }

                if (new Date() > +expiresIn || !token ) {
                    console.log('No token or invalid token')
                    vuexContext.dispatch('logOut')
                    return
                }

              vuexContext.commit('setToken', token)
            },
            logOut(vuexContext){
                vuexContext.commit('clearToken')
                Cookie.remove('jwt')
                Cookie.remove('tokenExpiration')
                if (process.client){
                    localStorage.removeItem('token')
                    localStorage.removeItem('tokenExpiration')
                }
                

            }
        },
        getters: {
            loadedPosts(state) {
                return state.loadedPosts
            },
            isAuthenticated(state) {
                return state.token !=null
            }
        }
    })

}

export default createStore