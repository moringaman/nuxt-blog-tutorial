export default function(context) {
    // if async code you must return a promise here i.e
    // return new Promise((resolve, reject)=> {
    //    code to execute like api call
    // })
    //.resolve(res => {});
    console.log('[MIDDLEWARE], Auth middleware running')
  if (!context.store.getters.isAuthenticated) {
        context.redirect('/admin/auth')
  }
    }